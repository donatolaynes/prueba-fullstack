En una PC con Docker, ejecutar los siguientes comandos en una terminal:

Alias temporal para facilitar la ejecución de comandos, que se ejecuta en la carpeta base del proyecto:

alias sail='bash ./vendor/bin/sail'


La 1ra vez que se instale el app se deberán ejecutar los siguientes comandos en sucesión:

docker run --rm \
    -v $(pwd):/opt \
    -w /opt \
    laravelsail/php80-composer:latest \
    composer install

sail artisan migrate
sail artisan db:seed
sail npm run prod

Luego en las siguientes ocasiones solo es necesario levantar los servicios con el siguiente comando:

sail up -d

Para ver el app, abrir en un navegador:

http://laravel.test:8082/login

usuario: admin@app.com
password: password

Los puertos de MySQL y httpd se pueden configurar en el archivo .env

Al finalizar el uso del app ejecutamos:

sail down
