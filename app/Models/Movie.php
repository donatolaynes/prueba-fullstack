<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

use App\Traits\MediaModel;

class Movie extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia, MediaModel;

    public $fillable = [
        'name',
        'published_at'
    ];

    /**
     * registered turns.
     */
    public function turns()
    {
        return $this->belongsToMany(Turn::class, 'movie_turn', 'movie_id', 'turn_id')
                    ->withTimestamps();
    }

}
