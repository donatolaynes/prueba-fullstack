<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Turn extends Model
{
    use HasFactory;

    public $fillable = [
        'when',
        'active'
    ];

    /**
     * assigned movies.
     */
    public function movies()
    {
        return $this->belongsToMany(Movie::class, 'movie_turn', 'turn_id', 'movie_id')
                    ->withTimestamps();
    }

}
