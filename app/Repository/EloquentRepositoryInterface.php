<?php

namespace App\Repository;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface EloquentRepositoryInterface
{
    /**
    * @param array $attributes
    * @return Model
    */
    public function create(array $attributes): Model;

    /**
    * @param $id
    * @param array $attributes
    * @return Model
    */
    public function update($id, array $attributes): Model;

    /**
    * @param $id
    * @return Model
    */
    public function find($id): ?Model;

    /**
    * @return Collection | LengthAwarePaginator
    */
    public function get(array $params = [], $pagination = null);

    /**
    * @return Collection;
    */
    public function all(): Collection;
}
