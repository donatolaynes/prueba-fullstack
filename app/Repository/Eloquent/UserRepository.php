<?php

namespace App\Repository\Eloquent;

use App\Models\User;
use App\Repository\UserRepositoryInterface;
use Illuminate\Support\Collection;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{

    /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    /**
    * Filter the records based on the parameters, and request other resources from the database
    *
    * @param array $params
    * @return Illuminate\Database\Eloquent\Model | Illuminate\Database\Eloquent\Builder
    */
    protected function setup($params){
        $res = $this->model;

        if(!empty($params['name'])){
            //Is this getting escaped correctly?
            $res->where('name', 'LIKE', '%'.$params['name'].'%');
        }
        if(!empty($params['email'])){
            $res->where('name', 'LIKE', '%'.$params['name'].'%');
        }

        return $res;
    }
}
