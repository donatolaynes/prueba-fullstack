<?php

namespace App\Repository\Eloquent;

use App\Models\Movie;
use App\Repository\MovieRepositoryInterface;
use Illuminate\Support\Collection;

class MovieRepository extends BaseRepository implements MovieRepositoryInterface
{

   /**
    * MovieRepository constructor.
    *
    * @param Movie $model
    */
    public function __construct(Movie $model)
    {
        parent::__construct($model);
    }

    /**
    * Filter the records based on the parameters, and request other resources from the database
    *
    * @param array $params
    * @return Illuminate\Database\Eloquent\Builder
    */
    protected function setup($params){
        $res = $this->model->with('turns');
        if(!empty($params['name'])){
            $res->where('name','LIKE', '%'.$params['name'].'%');
        }
        return $res;
    }

}
