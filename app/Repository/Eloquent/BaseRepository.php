<?php

namespace App\Repository\Eloquent;

use App\Repository\EloquentRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class BaseRepository implements EloquentRepositoryInterface
{
    /**
     * @var Model
     */
     protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
    * @param array $attributes
    *
    * @return Model
    */
    public function create(array $attributes): Model
    {
        return $this->model->create($attributes);
    }

    /**
    * @param $id
    * @param array $attributes
    *
    * @return Model
    */
    public function update($id, array $attributes): Model
    {
        return $this->model->where('id','=', $id)->update($attributes);
    }

    /**
    * @param $id
    * @return Model
    */
    public function find($id): ?Model
    {
        return $this->model->find($id);
    }

    /**
    * @param $id
    * @return boolean
    */
    public function delete($id): boolean
    {
        return $this->model->delete($id);
    }

    /**
    * @return Collection
    */
    public function all(): Collection
    {
        return $this->model->all();
    }

    /**
    * @param $params
    * @param $pagination
    * @return Collection | LengthAwarePaginator
    */
    public function get($params = [], $pagination = \NULL)
    {
        $query = $this->setup($params);
        return $this->handlePagination($query, $pagination);
    }

    /* utilities */

    /**
    * @param $query
    * @param $pagination
    * @return Collection | LengthAwarePaginator
    */
    protected function handlePagination($query, $pagination)
    {
       if ($pagination === \NULL)
       {
           return $query->get();
       }
       $collection = $query->paginate($pagination);
       return $collection;
    }

    /**
    * Filter the records based on the parameters, and request other resources from the database
    *
    * @param array $params
    * @return Illuminate\Database\Eloquent\Model | Illuminate\Database\Eloquent\Builder
    */
    protected abstract function setup($params);

}
