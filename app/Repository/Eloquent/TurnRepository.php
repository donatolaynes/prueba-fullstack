<?php

namespace App\Repository\Eloquent;

use App\Models\Turn;
use App\Repository\TurnRepositoryInterface;
use Illuminate\Support\Collection;

class TurnRepository extends BaseRepository implements TurnRepositoryInterface
{
    /**
    * TurnRepository constructor.
    *
    * @param Turn $model
    */
    public function __construct(Turn $model)
    {
        parent::__construct($model);
    }

    /**
    * Filter the records based on the parameters, and request other resources from the database
    *
    * @param array $params
    * @return Illuminate\Database\Eloquent\Model | Illuminate\Database\Eloquent\Builder
    */
    protected function setup($params){
        $res = $this->model;
        if(!empty($params['when'])){
            $res->where('when', 'LIKE', '%'.$params['when'].'%');
        }
        return $res;
    }
}
