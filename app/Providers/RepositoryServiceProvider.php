<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;


use App\Repository\EloquentRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use App\Repository\MovieRepositoryInterface;
use App\Repository\TurnRepositoryInterface;

use App\Repository\Eloquent\MovieRepository;
use App\Repository\Eloquent\TurnRepository;
use App\Repository\Eloquent\UserRepository;
use App\Repository\Eloquent\BaseRepository;

/**
* Class RepositoryServiceProvider
* @package App\Providers
*/
class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(MovieRepositoryInterface::class, MovieRepository::class);
        $this->app->bind(TurnRepositoryInterface::class, TurnRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
