<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Repository\MovieRepositoryInterface;

use Illuminate\Http\Request;
use App\Http\Requests\Movie\CreateRequest;
use App\Http\Requests\Movie\UpdateRequest;

use App\Traits\HandleUpload;

class MovieController extends Controller
{
    private $movieRepository;

    use HandleUpload;

    const MEDIA_GROUP = 'images';

    const MEDIA_DIR = 'movie-images';

    public function __construct(MovieRepositoryInterface $movieRepository)
    {
        $this->movieRepository = $movieRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = [
            'name' => $request->get('name')
        ];

        $records = $this->movieRepository->get($params, 10);

        return $records;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $data = $request->validated();

        $create = $this->movieRepository->create([
            'name' => $data['name'],
            'published_at' => $data['published_at']
        ]);

        //We store the turns
        $create->turns()->attach($data['turns']);

        //we attach an image if it was sent
        $baseName = 'movie-'.$update->id.'-'.time();
        $file = $this->handleUpload($request, 'image', self::MEDIA_DIR, $baseName);
        if($file !== null){
            $yourModel->simpleStoreMedia($file->path, $baseName.'.'.$file->extension, self::MEDIA_GROUP);
        }

        return [
            'ok' => \TRUE
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $record = $this->movieRepository->find($id);
        $record->image = $record->getFirstMediaUrl(self::MEDIA_GROUP);

        return [
            'record' => $record
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $data = $request->validated();

        $update = $this->movieRepository->update($id, [
            'name' => $data['name'],
            'published_at' => $data['published_at']
        ]);

        //We update the turns
        $update->turns()->sync($data['turns']);

        //we update the image if sent
        $baseName = 'movie-'.$update->id.'-'.time();
        $file = $this->handleUpload($request, 'image', self::MEDIA_DIR, $baseName);
        if($file !== \NULL){
            $update->simpleUpdateMedia($file->path, $baseName.'.'.$file->extension, self::MEDIA_GROUP);
        }

        return [
            'ok' => \TRUE
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = $this->movieRepository->find($id);
        if($record){
            $record->clearMediaCollection();
            $delete = $this->movieRepository->delete($id);
        }

        return [
            'ok' => \TRUE
        ];
    }
}
