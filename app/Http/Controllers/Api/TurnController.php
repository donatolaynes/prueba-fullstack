<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Repository\TurnRepositoryInterface;

use Illuminate\Http\Request;
use App\Http\Requests\Turn\CreateRequest;
use App\Http\Requests\Turn\UpdateRequest;

class TurnController extends Controller
{
    private $turnRepository;

    public function __construct(TurnRepositoryInterface $turnRepository)
    {
        $this->turnRepository = $turnRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->get('all')){
            return $this->turnRepository->all();
        }

        $params = [

        ];
        $records = $this->turnRepository->get($params, 10);

        return $records;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $create = $this->turnRepository->create($request->validated());

        return [
            'ok' => \TRUE
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return [
            'record' => $this->turnRepository->find($id)
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $update = $this->turnRepository->update($id, $request->validated());

        return [
            'ok' => \TRUE
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = $this->turnRepository->delete($id);

        return [
            'ok' => \TRUE
        ];
    }
}
