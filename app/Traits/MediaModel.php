<?php

namespace App\Traits;

trait MediaModel {

    function simpleStoreMedia($pathToFile, $fileName, $group){
        $this->addMedia($pathToFile)
            ->usingName($group)
            ->usingFileName($fileName)
            ->toMediaCollection();
    }

    function simpleUpdateMedia($pathToFile, $fileName, $group){
        $media = $this->getMedia($group);
        if(isset($media[0])){
            $prevPath = $media[0]->getPath();
            if($prevPath){
                @unlink($prevPath);
            }

            $media[0]->file_name = $fileName;
            $media[0]->save();
        } else {
            $this->simpleStoreMedia($pathToFile, $fileName, $group);
        }

    }

}
