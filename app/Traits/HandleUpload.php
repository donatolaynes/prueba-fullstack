<?php

namespace App\Traits;

use Illuminate\Http\Request;

class UploadedFile {
    public $path;
    public $extension;

    function __construct($path, $extension){
        $this->path = $path;
        $this->extension = $extension;
    }
}

/**
 *
 */
trait HandleUpload
{
    function handleUpload(Request $request, $name, $directory, $baseName) : ?UploadedFile{
        $file = $request->file($name);
        if($file){
            return new UploadedFile(
                $file->storeAs($directory, $baseName),
                \strtolower($file->extension())
            );
        }
        return \NULL;
    }
}
