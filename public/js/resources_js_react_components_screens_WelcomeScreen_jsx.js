(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_react_components_screens_WelcomeScreen_jsx"],{

/***/ "./resources/js/react/components/screens/WelcomeScreen.jsx":
/*!*****************************************************************!*\
  !*** ./resources/js/react/components/screens/WelcomeScreen.jsx ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");


/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("div", {
    className: "bg-white h-full pt-8",
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("h2", {
      className: "font-semibold text-xl text-gray-800 leading-tight text-center",
      children: "Dashboard aplicaci\xF3n pel\xEDculas"
    })
  });
}

/***/ })

}]);