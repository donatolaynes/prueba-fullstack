<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!User::count()){
            User::factory(1)->create([
                'name' => 'Admin Peliculas',
                'email' => 'admin@app.com'
            ]);
            User::factory(4)->create();
        }
    }
}
