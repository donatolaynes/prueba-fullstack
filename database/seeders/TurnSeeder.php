<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Turn;

class TurnSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!Turn::count()){
            Turn::factory(4)->create();
        }
    }
}
