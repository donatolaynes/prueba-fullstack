import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import TableWrapper from '../table/TableWrapper';

import usersObj from '../../data/store/ducks/users';

import { Link } from 'react-router-dom';

function Actions({record, toggleRecord, deleteRecord}){
    return (
        <React.Fragment>
            <Link to={"/users/"+record.id}><i className="fa fa-edit"></i></Link>
            <a href="#" onClick={deleteRecord}>
                <i className="fa fa-trash"></i>
            </a>
        </React.Fragment>
    );
}

export default function(){
    const users = useSelector((state) => state.users.some);
    const dispatch = useDispatch();

    const deleteRecord = useCallback(function(record){

    });

    const getRecords = useCallback((params) => {
        dispatch(usersObj.actions.someRequest(params))
    });

    const columns = [
        {
            Header: "Id",
            className: 'text-center',
            accessor: "id",
            width: 50,
            filterable: false,
            sortable: false
        },
        {
          Header: "Nombre",
          className: 'text-left',
          accessor: "name",
          filterable: false,
          sortable: false
        },
        {
            Header: "Email",
            className: 'text-left',
            accessor: "email",
            filterable: false,
            sortable: false
        },
        {
          Header: "Acciones",
          className: 'text-center table-actions',
          filterable: false,
          width: 140,
          sortable: false,
          Cell: function(row){
            return <Actions
                record={row.original}
                deleteRecord={deleteRecord} />
          }
        },
    ];

    return (
        <div className="bg-white h-full pt-8">
            <h2 className="font-semibold text-xl text-gray-800 leading-tight text-center">
                Administradores
            </h2>

            <p className="px-12 pb-4 mb-5">
                <Link className="float-right" to="/users/new">
                    <i className="fa fa-plus"></i> Crear nuevo administrador
                </Link>
            </p>
            <div className="clear-both px-12">
                <TableWrapper
                columns={columns}
                {...users}
                getAction={getRecords}
                />
            </div>

        </div>
    );
}
