import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import TableWrapper from '../table/TableWrapper';

import turnsObj from '../../data/store/ducks/turns';

import { Link } from 'react-router-dom';

function Actions({record, toggleRecord, deleteRecord}){
    return (
        <React.Fragment>
            <Link to={"/turns/"+record.id}><i className="fa fa-edit"></i></Link>
            <a href="#" onClick={toggleRecord}>{
                record.active ?
                    <i className="text-success fa fa-toggle-on"></i> :
                    <i className="text-danger fa fa-toggle-off"></i>
            }</a>
            <a href="#" onClick={deleteRecord}>
                <i className="fa fa-trash"></i>
            </a>
        </React.Fragment>
    );
}

export default function(){
    const turns = useSelector((state) => state.turns.some);
    const dispatch = useDispatch();

    const toggleRecord = useCallback(function(record){

    });

    const deleteRecord = useCallback(function(record){

    });

    const getRecords = useCallback((params) => {
        dispatch(turnsObj.actions.someRequest(params))
    });

    const columns = [
        {
            Header: "Id",
            className: 'text-center',
            accessor: "id",
            width: 50,
            filterable: false,
            sortable: false
        },
        {
            Header: "Horario",
            className: 'text-center',
            filterable: false,
            sortable: false,
            Cell: function(row){
                return row.original.when.substring(0, 5);
            }
        },
        {
            Header: "Estado",
            className: 'text-center',
            width: 120,
            filterable: false,
            sortable: false,
            Cell: function(row){
                return row.original.active ? 'Activo' : 'Inactivo';
            }
        },
        {
          Header: "Acciones",
          className: 'text-center table-actions',
          filterable: false,
          width: 140,
          sortable: false,
          Cell: function(row){
            return <Actions
                record={row.original}
                toggleRecord={toggleRecord}
                deleteRecord={deleteRecord} />
          }
        },
    ];

    return (
        <div className="bg-white h-full pt-8">
            <h2 className="font-semibold text-xl text-gray-800 leading-tight text-center">
                Turnos
            </h2>
            <p className="px-12 pb-4 mb-5">
                <Link className="float-right" to="/turns/new">
                    <i className="fa fa-plus"></i> Crear nuevo turno
                </Link>
            </p>
            <div className="clear-both px-12">
                <TableWrapper
                columns={columns}
                {...turns}
                getAction={getRecords}
                />
            </div>
        </div>
    )
}
