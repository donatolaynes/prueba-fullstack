import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import turnsObj from '../../data/store/ducks/turns';

import { useForm } from "react-hook-form";

import { Link, useParams } from 'react-router-dom';

export default function(){
    const params = useParams();

    const turn = useSelector((state) => state.turns.one);
    const dispatch = useDispatch();

    useEffect(function(){
        if(params.id){
            dispatch(turnsObj.actions.findRequest(params.id));
        } else {
            dispatch(turnsObj.actions.resetRecord());
        }
    }, []);

    const { register, handleSubmit, watch, errors } = useForm();
    const onSubmit = data => console.log(data);

    return (
        <div className="bg-white h-full pt-8">
            <h2 className="font-semibold text-xl text-gray-800 leading-tight text-center">
                Agregar / Editar Turnos
            </h2>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 flex flex-col my-2">
                    <div className="-mx-3 md:flex mb-6">
                        <div className="md:w-1/2 px-3 mb-6 md:mb-0">
                            <label className="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" htmlFor="grid-first-name">
                                Horario
                            </label>
                            <input className="appearance-none block w-full bg-grey-lighter text-grey-darker border border-red rounded py-3 px-4 mb-3" id="grid-first-name" type="time" placeholder="Jane" />

                        </div>
                        <div className="md:w-1/2 px-3">
                            <label className="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" htmlFor="grid-last-name">
                                Activo?
                            </label>
                            <input type="checkbox" className="inline-block mt-4 ml-2" />
                        </div>
                    </div>
                    <div className="mt-2">
                        <Link to="/users" className="inline-block pt-3"><i className="fa fa-arrow-left"></i> Regresar</Link>
                        <button className="float-right border border-indigo-500 bg-indigo-500 text-white rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-indigo-600 focus:outline-none focus:shadow-outline">Guardar</button>
                    </div>
                </div>
            </form>

        </div>
    );
};
