import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import moviesObj from '../../data/store/ducks/movies';
import turnsObj from '../../data/store/ducks/turns';

import { useForm } from "react-hook-form";

import { Link, useParams } from 'react-router-dom';

import ImageUploader from "react-images-upload";

export default function(){

    const params = useParams();

    const movie = useSelector((state) => state.movies.one);
    const turns = useSelector((state) => state.turns.all);
    const dispatch = useDispatch();

    useEffect(function(){
        dispatch(turnsObj.actions.allRequest({}));
        if(params.id){
            dispatch(moviesObj.actions.findRequest(params.id));
        } else {
            dispatch(moviesObj.actions.resetRecord());
        }
    }, []);

    //Form
    const onDrop = function(pictureFiles, pictureDataURLs){

    };
    const { register, handleSubmit, watch, errors } = useForm();
    const onSubmit = data => console.log(data);

    return (
        <div className="bg-white h-full pt-8">
            <h2 className="font-semibold text-xl text-gray-800 leading-tight text-center">
                Agregar / Editar Películas
            </h2>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 flex flex-col my-2">
                    <div className="-mx-3 md:flex mb-6">
                        <div className="md:w-1/2 px-3 mb-6 md:mb-0">
                            <label className="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" htmlFor="grid-first-name">
                                Nombre de la película
                            </label>
                            <input className="appearance-none block w-full bg-grey-lighter text-grey-darker border border-red rounded py-3 px-4 mb-3"
                                name="name" id="name" type="text" placeholder="" />
                        </div>
                        <div className="md:w-1/2 px-3">
                            <label className="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" htmlFor="grid-last-name">
                                Fecha de publicación
                            </label>
                            <input className="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4"
                                id="published_at" type="date" placeholder="" />
                        </div>
                    </div>
                    <div className="clear-both">
                        <strong className="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2">
                            Imagen
                        </strong>
                    </div>
                    <ImageUploader
                        singleImage={true}
                        withIcon={true}
                        label="Tamaño máximo: 5mb, tipo: jpg,gif,png"
                        buttonText="Selecciona una imagen"
                        buttonType="button"
                        onChange={onDrop}
                        imgExtension={[".jpg", ".jpeg", ".png", ".gif"]}
                        maxFileSize={5242880}
                    />

                    <div className="clear-both">
                        <strong className="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2">
                            Turnos disponibles
                        </strong>
                    </div>
                    <div className="clear-both"></div>
                    <div className="-mx-3 md:flex mb-2">

                        {
                            turns.records.map(item => {
                                return (
                                    <div className="md:w-1/2 px-3">
                                        <input type="checkbox" /> {item.when.substring(0,5)}
                                    </div>
                                )
                            })
                        }
                    </div>
                    <div className="mt-2">
                        <Link to="/movies" className="inline-block pt-3"><i className="fa fa-arrow-left"></i> Regresar</Link>
                        <button className="float-right border border-indigo-500 bg-indigo-500 text-white rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-indigo-600 focus:outline-none focus:shadow-outline">Guardar</button>
                    </div>

                </div>
            </form>

        </div>
    );
};
