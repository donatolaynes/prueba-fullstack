import React from 'react';

import ReactTable from "react-table";
import { pageToOffset } from '../../utils/table';

export default function({page, total, rowsText, orderBy, order, search, loading, columns, records, getAction, defaultFiltered = []}){

    const handleFetchData = function(state, instance){

        const sorted = state.sorted[0] || {id: orderBy, desc: order === 'desc'};
        const search_ = state.filtered || search;

        getAction({
            page: state.page,
            orderBy: sorted.id,
            order: sorted.desc ? 'desc' : 'asc',
            search: search_
        });
    }

    let pages = Math.ceil(total/10);

    return (
        <ReactTable
            manual
            page={page}
            pages={pages || 0}
            loading={loading}
            columns={columns}
            defaultPageSize={10}
            pageSizeOptions={[10]}
            defaultFiltered={defaultFiltered}
            showPaginationBottom={true}
            loadingText="Cargando..."
            noDataText="Sin registros"
            previousText="Anterior"
            nextText="Siguiente"
            pageText="Página"
            rowsText={rowsText || "registros"}
            className="-striped -highlight"
            data={records}
            filterable
            multiSort={false}
            onFetchData={handleFetchData}
        />
    );

}

