import React from 'react';

import { Link, useLocation } from 'react-router-dom';

const links = [
    {id: 'dashboard', to: '/', name: 'Dashboard'},
    {id: 'movies', to: '/movies', name: 'Películas'},
    {id: 'turns', to: '/turns', name: 'Turnos'},
    {id: 'users', to: '/users', name: 'Administradores'},
    {id: 'profile', to: '/profile', name: 'Perfil'}
];

const MenuItem = function({path, link}){
    let bgClass = link.to === path || ( link.to.length !== 1 && path.indexOf(link.to) === 0) ? 'bg-black' : 'hover:bg-black';
    return (
        <div className="group relative sidebar-item">
            <Link to={link.to} className={"block text-center shadow-light py-4 border-l-4 border-transparent "+ bgClass}>
                <span className="text-white text-xs">{link.name}</span>
            </Link>
        </div>
    );
}

export default function(){

    const location = useLocation();
    //console.log("Current location", location);

    const logout = function(){
        document.getElementById('logout-form').submit();
    };

    return (
        <div className="sidebar bg-grey-darkest relative h-full min-h-screen">
            <div>
                {links.map(link => <MenuItem key={"sidebar-route-"+link.id} path={location.pathname} link={link} />)}
                <div className="group relative sidebar-item">
                    <button onClick={logout} type="button" className="block w-full text-center shadow-light py-4 border-l-4 border-transparent hover:bg-black">
                        <span className="text-white text-xs text-center">Cerrar sesión</span>
                    </button>
                </div>
            </div>
        </div>
    )
}
