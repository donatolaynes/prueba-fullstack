import React, { lazy, Suspense } from 'react';
import { Route, Switch } from "react-router-dom";
import Loading from './Loading';

const Home = lazy(() => import("../screens/WelcomeScreen"));

const Movies = lazy(() => import("../screens/MoviesScreen"));
const Turns = lazy(() => import("../screens/TurnsScreen"));
const Users = lazy(() => import("../screens/UsersScreen"));

const MovieForm = lazy(() => import("../screens/MovieFormScreen"));
const TurnForm = lazy(() => import("../screens/TurnFormScreen"));
const UserForm = lazy(() => import("../screens/UserFormScreen"));

const Profile = lazy(() => import("../screens/ProfileScreen"));

export default function(){
    return (
        <Suspense fallback={<Loading />}>
            <Switch>
                <Route exact path="/" component={Home}></Route>
                <Route exact path="/movies" component={Movies}></Route>
                <Route exact path="/movies/new" component={MovieForm}></Route>
                <Route exact path="/movies/:id" component={MovieForm}></Route>
                <Route exact path="/turns" component={Turns}></Route>
                <Route exact path="/turns/new" component={TurnForm}></Route>
                <Route exact path="/turns/:id" component={TurnForm}></Route>
                <Route exact path="/users" component={Users}></Route>
                <Route exact path="/users/new" component={UserForm}></Route>
                <Route exact path="/users/:id" component={UserForm}></Route>
                <Route exact path="/profile" component={Profile}></Route>
            </Switch>
        </Suspense>
    );
}
