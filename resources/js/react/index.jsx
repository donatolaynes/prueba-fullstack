import React from 'react';
import ReactDOM from 'react-dom';

import Routes from './components/global/Routes';
import Sidebar from './components/global/Sidebar';

import "react-table/react-table.css";
import { Router } from 'react-router-dom';
import { History } from './utils/history';

import { Provider } from "react-redux";
import { configureStore } from './data/store';

const App = function(){
    return (
        <Provider store={configureStore()}>
            <Router history={History}>
                <Sidebar />
                <Routes />
            </Router>
        </Provider>
    )
};

ReactDOM.render(
    <App />,
    document.getElementById('root')
  );
