import instance from "./base";
import { searchFieldsToQueryArray } from "../../utils/table";

export default class {
    static async getTurns({page, orderBy, order, search, getAll}){
        let request = [];
        if(page){
            request.push('page='+(page+1) );
        }
        if(orderBy){
            request.push('orderBy='+orderBy);
            request.push('order='+order);
        }
        if(getAll){
            request.push('all=1');
        }
        if(search && search.length){
            request = searchFieldsToQueryArray(search, request);
        }
        const response = await instance.get(
            'api/turns' + (request.length ? '?'+request.join('&') : '')
        );
        return response.data;
    }

    static async getTurn(id){
        const response = await instance.get(
            'api/turns/' + id
        );
        return response.data;
    }

    static async createTurn(props){
        const response = await instance.post(
            'api/turns', {
                name: props.name
            }
        );
        return response.data;
    }

    static async updateTurn(id, props){
        const response = await instance.put(
            'api/turns/'+id, {
                name: props.name
            }
        );
        return response.data;
    }

    static async deleteTurn(id){
        const response = await instance.delete(
            'api/turns/' + id
        );
        return response.data;
    }
}
