import instance from "./base";

export default class {

    static async getMovies({page, orderBy, order, search, getAll}){
        let request = [];
        if(page){
            request.push('page='+(page+1) );
        }
        if(orderBy){
            request.push('orderBy='+orderBy);
            request.push('order='+order);
        }
        if(getAll){
            request.push('all=1');
        }
        if(search && search.length){
            request = searchFieldsToQueryArray(search, request);
        }
        const response = await instance.get(
            'api/movies' + (request.length ? '?'+request.join('&') : '')
        );
        return response.data;
    }

    static async getMovie(id){
        const response = await instance.get(
            'api/movies/' + id
        );
        return response.data;
    }

    static async createMovie(props){
        const response = await instance.post(
            'api/movies', {
                name: props.name
            }
        );
        return response.data;
    }

    static async updateMovie(id, props){
        const response = await instance.put(
            'api/movies/'+id, {
                name: props.name
            }
        );
        return response.data;
    }

    static async deleteMovie(id){
        const response = await instance.delete(
            'api/movies/' + id
        );
        return response.data;
    }

}
