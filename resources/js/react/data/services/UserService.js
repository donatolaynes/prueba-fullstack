import instance from "./base";
import { searchFieldsToQueryArray } from "../../utils/table";

export default class {
    static async getUsers({page, orderBy, order, search, getAll}){
        let request = [];
        if(page){
            request.push('page='+(page+1) );
        }
        if(getAll){
            request.push('all=1');
        }
        if(orderBy){
            request.push('orderBy='+orderBy);
            request.push('order='+order);
        }
        if(search && search.length){
            request = searchFieldsToQueryArray(search, request);
        }
        const response = await instance.get(
            'api/users' + (request.length ? '?'+request.join('&') : '')
        );
        return response.data;
    }

    static async getUser(id){
        const response = await instance.get(
            'api/users/' + id
        );
        return response.data;
    }

    static async createUser(props){
        const response = await instance.post(
            'api/users', {
                name: props.name
            }
        );
        return response.data;
    }

    static async updateUser(id, props){
        const response = await instance.put(
            'api/users/'+id, {
                name: props.name
            }
        );
        return response.data;
    }

    static async deleteUser(id){
        const response = await instance.delete(
            'api/users/' + id
        );
        return response.data;
    }
}
