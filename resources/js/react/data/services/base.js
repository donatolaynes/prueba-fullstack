const urls = {
    dev: 'http://laravel.test:8082',
    testing: 'http://laravel.test:8082',
    prod: 'http://laravel.test:8082',
};

let env = 'dev';

if(process.env.NODE_ENV === 'production'){
    env = 'testing';
}

export const currentEnvironment = env;

export const baseUrl = urls[currentEnvironment];

const instance = window.axios.create({
    baseURL : baseUrl,
    withCredentials: true,
    timeout: 1000000
});

instance.interceptors.response.use(
    function(response){
        if(response.data){

        }
        return Promise.resolve(response);
    },
    function(error){
        if(error.message.indexOf('401') !== -1){
            //TODO, standarize
            window.location.href = 'http://'+window.location.hostname+':'+ window.location.port+'/login';
        }
        return Promise.reject(error);
    }
);

export default instance;
