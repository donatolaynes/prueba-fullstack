import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import { composeWithDevTools } from 'redux-devtools-extension';
import Reducers from './reducers';
import Sagas from './sagas';

import logger from 'redux-logger';

export function configureStore(initialState) {

    const sagaMiddleware = createSagaMiddleware();

    const middleware = [
        sagaMiddleware
    ];

    if (process.env.NODE_ENV !== 'production') {
        middleware.push(logger);
    }

    const Store = createStore(
        Reducers,
        initialState,
        composeWithDevTools (applyMiddleware(...middleware))
    );
    sagaMiddleware.run(Sagas);
    return Store;
}
