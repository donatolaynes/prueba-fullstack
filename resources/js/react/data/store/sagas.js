import {all} from 'redux-saga/effects';

import usersObj from './ducks/users';
import turnsObj from './ducks/turns';
import moviesObj from './ducks/movies';

import { alertSagas } from './classic/alert';

export default function* Sagas(){
    return yield all([
        alertSagas,
        usersObj.sagas,
        turnsObj.sagas,
        moviesObj.sagas
    ]);
}
