import { takeLatest, all, put, call } from "redux-saga/effects";
import { History } from "../../../utils/history";
import { createDucksCrud } from "../../../utils/redux";
import UserService from "../../services/UserService";
import { ALERT_ERROR, setAlertAction } from "../classic/alert";

const usersState = createDucksCrud('test','users','id','id','desc', {name: '', email: '', password: '', password_confirm: ''});

function* watchGetRecords(action){
    action = action || {};
    const payload = action.payload || {};
    try {
        const results = yield call(UserService.getUsers, payload);
        //console.log("users", results);
        yield put(usersState.actions.someSuccess({
            records: results.data,
            total: results.total
        }));
    } catch(e){
        console.warn(e);
        yield put(usersState.actions.someError(""));
        yield put(setAlertAction({
            type: ALERT_ERROR,
            text: "Hubo un problema al momento de cargar los usuarios"
        }));
    }
}

function* watchFindRecord(action){
    action = action || {};
    const payload = action.payload || null;

    try {
        const results = yield call(UserService.getUser, payload);
        console.log("Data", results);

    } catch(e){
        console.warn(e);
        yield put(usersState.actions.findError(""));
        yield put(setAlertAction({
            type: ALERT_ERROR,
            text: "Hubo un problema al momento de cargar un Usuario"
        }));
    }
}

function* watchCreateRecord(action){
    try {
        action = action || {};
        const payload = action.payload || {};

        const results = yield call(UserService.createUser, payload);
        console.log("Data", results);

    } catch(e){
        console.warn(e);
        yield put(usersState.actions.createError());
        yield put(setAlertAction({
            type: ALERT_ERROR,
            text: "Hubo un problema al momento de crear un Usuario"
        }));
    }
}

function* watchUpdateRecord(action){
    try {
        action = action || {};
        const payload = action.payload || {};

        const results = yield call(UserService.updateUser, payload.record?.id, payload.record);
        console.log("Data", results);

    } catch(e){
        console.warn(e);
        yield put(usersState.actions.updateError());
        yield put(setAlertAction({
            type: ALERT_ERROR,
            text: "Hubo un problema al momento de actualizar un Usuario"
        }));
    }
}

function* watchDeleteRecord(action){
    try {
        action = action || {};
        const payload = action.payload || {};

        const results = yield call(UserService.deleteUser, payload.record?.id);
        console.log("Data", results);

    } catch(e){
        console.warn(e);
        yield put(usersState.actions.deleteError());
        yield put(setAlertAction({
            type: ALERT_ERROR,
            text: "Hubo un problema al momento de borrar un usuario"
        }));
    }
}

export default {
    actions: usersState.actions,
    reducer: usersState.reducer,
    filter: usersState.filter,
    sagas: all([
        //takeLatest(UsersState.name + '.ALL.REQUEST', ),
        takeLatest(usersState.name + '.SOME.REQUEST', watchGetRecords),
        takeLatest(usersState.name + '.FIND.REQUEST', watchFindRecord),
        takeLatest(usersState.name + '.CREATE.REQUEST', watchCreateRecord),
        takeLatest(usersState.name + '.UPDATE.REQUEST', watchUpdateRecord),
        takeLatest(usersState.name + '.DELETE.REQUEST', watchDeleteRecord),
    ])
};
