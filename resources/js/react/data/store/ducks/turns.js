import { takeLatest, all, put, call } from "redux-saga/effects";
import { History } from "../../../utils/history";
import { createDucksCrud } from "../../../utils/redux";
import TurnService from "../../services/TurnService";
import { ALERT_ERROR, setAlertAction } from "../classic/alert";

const turnsState = createDucksCrud('test','turns','id','id','desc', {id: null, when: '', active: true });

function* watchAllRecords(action){
    action = action || {};
    const payload = action.payload || {};
    payload.getAll = true;
    try {
        const results = yield call(TurnService.getTurns, payload);
        //just "results"
        yield put(turnsState.actions.allSuccess({
            records: results
        }));
    } catch(e){
        console.warn(e);
        yield put(turnsState.actions.allError(""));
        yield put(setAlertAction({
            type: ALERT_ERROR,
            text: "Hubo un problema al momento de cargar los turnos"
        }));
    }
}

function* watchGetRecords(action){
    action = action || {};
    const payload = action.payload || {};
    try {
        const results = yield call(TurnService.getTurns, payload);
        yield put(turnsState.actions.someSuccess({
            records: results.data,
            total: results.total
        }));
    } catch(e){
        console.warn(e);
        yield put(turnsState.actions.someError(""));
        yield put(setAlertAction({
            type: ALERT_ERROR,
            text: "Hubo un problema al momento de cargar los turnos"
        }));
    }
}

function* watchFindRecord(action){
    action = action || {};
    const payload = action.payload || null;

    try {
        const results = yield call(TurnService.getTurn, payload);
        console.log("Data", results);

    } catch(e){
        console.warn(e);
        yield put(turnsState.actions.findError(""));
        yield put(setAlertAction({
            type: ALERT_ERROR,
            text: "Hubo un problema al momento de cargar un turno"
        }));
    }
}

function* watchCreateRecord(action){
    try {
        action = action || {};
        const payload = action.payload || {};

        const results = yield call(TurnService.createTurn, payload);
        console.log("Data", results);

    } catch(e){
        console.warn(e);
        yield put(turnsState.actions.createError());
        yield put(setAlertAction({
            type: ALERT_ERROR,
            text: "Hubo un problema al momento de crear un turno"
        }));
    }
}

function* watchUpdateRecord(action){
    try {
        action = action || {};
        const payload = action.payload || {};

        const results = yield call(TurnService.updateTurn, payload.record?.id, payload.record);
        console.log("Data", results);

    } catch(e){
        console.warn(e);
        yield put(turnsState.actions.updateError());
        yield put(setAlertAction({
            type: ALERT_ERROR,
            text: "Hubo un problema al momento de actualizar un turno"
        }));
    }
}

function* watchDeleteRecord(action){
    try {
        action = action || {};
        const payload = action.payload || {};

        const results = yield call(TurnService.deleteTurn, payload.record?.id);
        console.log("Data", results);

    } catch(e){
        console.warn(e);
        yield put(turnsState.actions.deleteError());
        yield put(setAlertAction({
            type: ALERT_ERROR,
            text: "Hubo un problema al momento de borrar una película"
        }));
    }
}

export default {
    actions: turnsState.actions,
    reducer: turnsState.reducer,
    filter: turnsState.filter,
    sagas: all([
        takeLatest(turnsState.name + '.ALL.REQUEST', watchAllRecords),
        takeLatest(turnsState.name + '.SOME.REQUEST', watchGetRecords),
        takeLatest(turnsState.name + '.FIND.REQUEST', watchFindRecord),
        takeLatest(turnsState.name + '.CREATE.REQUEST', watchCreateRecord),
        takeLatest(turnsState.name + '.UPDATE.REQUEST', watchUpdateRecord),
        takeLatest(turnsState.name + '.DELETE.REQUEST', watchDeleteRecord),
    ])
};
