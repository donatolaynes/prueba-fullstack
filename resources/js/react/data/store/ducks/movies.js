import { takeLatest, all, put, call } from "redux-saga/effects";
import { History } from "../../../utils/history";
import { createDucksCrud } from "../../../utils/redux";
import MovieService from "../../services/MovieService";
import { ALERT_ERROR, setAlertAction } from "../classic/alert";

const moviesState = createDucksCrud('test','movies','id','id','desc', {id: null, name: '', published_at: '', active: true});

function* watchGetRecords(action){
    action = action || {};
    const payload = action.payload || {};
    try {
        //console.log("Before get movies", action, MovieService.getMovies);
        const results = yield call(MovieService.getMovies, payload);
        yield put(moviesState.actions.someSuccess({
            records: results.data,
            total: results.total
        }));
    } catch(e){
        console.warn(e);
        yield put(moviesState.actions.someError(""));
        yield put(setAlertAction({
            type: ALERT_ERROR,
            text: "Hubo un problema al momento de cargar las películas"
        }));
    }
}

function* watchFindRecord(action){
    action = action || {};
    const payload = action.payload || null;

    try {
        const results = yield call(MovieService.getMovie, payload);
        console.log("Data", results);

    } catch(e){
        console.warn(e);
        yield put(moviesState.actions.findError(""));
        yield put(setAlertAction({
            type: ALERT_ERROR,
            text: "Hubo un problema al momento de cargar una película"
        }));
    }
}

function* watchCreateRecord(action){
    try {
        action = action || {};
        const payload = action.payload || {};

        const results = yield call(MovieService.createMovie, payload.record);
        console.log("Data", results);

    } catch(e){
        console.warn(e);
        yield put(moviesState.actions.createError());
        yield put(setAlertAction({
            type: ALERT_ERROR,
            text: "Hubo un problema al momento de crear una película"
        }));
    }
}

function* watchUpdateRecord(action){
    try {
        action = action || {};
        const payload = action.payload || {};

        const results = yield call(MovieService.updateMovie, payload.record?.id, payload.record);
        console.log("Data", results);

    } catch(e){
        console.warn(e);
        yield put(moviesState.actions.updateError());
        yield put(setAlertAction({
            type: ALERT_ERROR,
            text: "Hubo un problema al momento de actualizar una película"
        }));
    }
}

function* watchDeleteRecord(action){
    try {
        action = action || {};
        const payload = action.payload || {};

        const results = yield call(MovieService.deleteMovie, payload.record?.id);
        console.log("Data", results);

    } catch(e){
        console.warn(e);
        yield put(moviesState.actions.deleteError());
        yield put(setAlertAction({
            type: ALERT_ERROR,
            text: "Hubo un problema al momento de borrar una película"
        }));
    }
}

export default {
    actions: moviesState.actions,
    reducer: moviesState.reducer,
    filter: moviesState.filter,
    sagas: all([
        takeLatest(moviesState.name + '.SOME.REQUEST', watchGetRecords),
        takeLatest(moviesState.name + '.FIND.REQUEST', watchFindRecord),
        takeLatest(moviesState.name + '.CREATE.REQUEST', watchCreateRecord),
        takeLatest(moviesState.name + '.UPDATE.REQUEST', watchUpdateRecord),
        takeLatest(moviesState.name + '.DELETE.REQUEST', watchDeleteRecord),
    ])
};
