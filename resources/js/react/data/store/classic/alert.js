import { takeLatest, put, delay, all } from "redux-saga/effects";

export const ALERT_NONE = 0;
export const ALERT_SUCCESS = 1;
export const ALERT_ERROR = 2;
export const ALERT_WARNING = 3;
export const ALERT_INFO = 4;

const SET_ALERT = '@test/alert.SET';
const RESET_ALERT = '@test/alert.RESET';

export const alertFilter = [
    SET_ALERT,
    RESET_ALERT
];

export function setAlertAction(alert){
    return {
        type: SET_ALERT,
        payload: {
            alert: alert
        }
    }
}

export function resetAlertAction(alert){
    return {
        type: RESET_ALERT
    }
}

const defaultAlert = {
    type: ALERT_NONE,
    text: ''
};

export function alertReducer(state = defaultAlert, action){
    switch(action.type){
        case RESET_ALERT:
            return {
                ... defaultAlert
            };
        case SET_ALERT:
            return {
                ...action.payload.alert
            };
        default:
            return state;
    }
}

function* watchSetAlert(action){
    if(action.payload.alert.type !== ALERT_NONE){
        alert(action.payload.alert.text);
        delay(5000);
        yield put(resetAlertAction());
    }
}

export const alertSagas = all([
    takeLatest(SET_ALERT, watchSetAlert)
]);
