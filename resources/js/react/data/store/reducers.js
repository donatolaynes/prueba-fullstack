import { combineReducers } from 'redux';
import { filterActions } from 'redux-ignore';

import usersObj from './ducks/users';
import turnsObj from './ducks/turns';
import moviesObj from './ducks/movies';
import { alertFilter, alertReducer } from './classic/alert';

const Reducers = combineReducers({
    alert: filterActions(alertReducer, alertFilter),
    users: filterActions(usersObj.reducer, usersObj.filter),
    turns: filterActions(turnsObj.reducer, turnsObj.filter),
    movies: filterActions(moviesObj.reducer, moviesObj.filter),
});

export default Reducers;
