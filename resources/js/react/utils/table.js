export function pageToOffset(page, limit){
    if(page < 0) page = 0;
    return page * limit;
}

export function searchFieldsToQueryArray(objs, res = []){
    for(let i=0; i<objs.length; i++){
        let obj = objs[i];
        res.push("search["+obj.id+"]="+encodeURIComponent(obj.value));
    }
    return res;
}
