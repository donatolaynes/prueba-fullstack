function getActionPrefix(namespace, mod, action){
    const prefix = `@${namespace}/${mod}.${action}`;
    return prefix;
}

const valid_actions = ['ALL','SOME','FIND','CREATE','UPDATE','DELETE'];

//No cache invalidation, no separate error messages, no sagas, only CRUD actions

export function createDucksCrud(namespace, mod, orderBy='id', filterBy='id', order='desc', record_default={}){
    const filter = [];

    const actions = {};

    const ducks_name = `@${namespace}/${mod}`;

    for(let i=0; i<valid_actions.length; i++){
        const action = valid_actions[i];
        const name = getActionPrefix(namespace, mod, action);
        filter.push(name+'.REQUEST', name+'.SUCCESS', name+'.ERROR');

        const lc = action.toLowerCase();

        actions[lc+'Request'] = function(payload, extra = {}){
            return {
                type: name+'.REQUEST',
                payload: payload,
                extra: extra
            };
        };
        actions[lc+'Success'] = function(payload, extra = {}){
            return {
                type: name+'.SUCCESS',
                payload: payload,
                extra: extra
            };
        };
        actions[lc+'Error'] = function(payload, extra = {}){
            return {
                type: name+'.ERROR',
                payload: payload,
                extra: extra
            };
        };
    }
    filter.push(ducks_name+'.SET');

    actions.setRecord = function(payload){
        return {
            type: ducks_name+'.SET',
            payload: payload
        };
    };
    filter.push(ducks_name+'.SET');
    actions.resetRecord = function(){
        return {
            type: ducks_name+'.RESET'
        };
    };

    const default_state = {
        some: {
            records: [],
            loading: false,
            error: '',
            total: 0,
            page: 0,
            perPage: 10,
            orderBy: orderBy,
            order: order,
            search: []
        },
        all: {
            records: [],
            loading: false,
            error: '',
            total: 0,
            orderBy: orderBy,
            order: order,
            search: []
        },
        single: {
            loading: false,
            error: '',
            record: record_default
        }
    };

    const reducer = function(state = default_state, action){

        switch(action.type){
            case ducks_name + '.ALL.REQUEST':
                if(action.extra.noReset){
                    return {
                        ...state,
                        all: {
                            records: state.all.records,
                            loading: true,
                            error: ''
                        }
                    };
                }
                return {
                    ...state,
                    all: {
                        records: [],
                        loading: true,
                        error: ''
                    }
                };
            case ducks_name + '.ALL.SUCCESS':
                return {
                    ...state,
                    all: {
                        ...action.payload,
                        loading: false,
                        error: ''
                    }
                };
            case ducks_name + '.ALL.ERROR':
                return {
                    ...state,
                    all: {
                        records: state.all.records,
                        loading: false,
                        error: action.payload
                    }
                };
            case ducks_name + '.SOME.REQUEST':
                if(action.extra.noReset){
                    return {
                        ...state,
                        some: {
                            records: state.some.records,
                            loading: true,
                            error: ''
                        }
                    };
                }
                return {
                    ...state,
                    some: {
                        records: [],
                        loading: true,
                        error: ''
                    }
                };
            case ducks_name + '.SOME.SUCCESS':
                return {
                    ...state,
                    some: {
                        ...action.payload,
                        loading: false,
                        error: ''
                    }
                };
            case ducks_name + '.SOME.ERROR':
                return {
                    ...state,
                    some: {
                        records: state.some.records,
                        loading: false,
                        error: action.payload
                    }
                };
            case ducks_name + '.FIND.REQUEST':
                return {
                    ...state,
                    single: {
                        loading: true,
                        error: '',
                        record: {
                            ...record_default
                        }
                    }
                };
            case ducks_name + '.FIND.SUCCESS':
                //console.log("Got single", ducks_name, action.payload);
                return {
                    ...state,
                    single: {
                        error: '',
                        loading: false,
                        record: action.payload
                    }
                };
            case ducks_name + '.FIND.ERROR':
                return {
                    ...state,
                    single: {
                        error: action.payload,
                        loading: false,
                        record: state.single.record
                    }
                };
            case ducks_name + '.CREATE.REQUEST':
                return {
                    ...state,
                    single: {
                        error: '',
                        loading: true,
                        record: state.single.record
                    }
                };
            case ducks_name + '.CREATE.SUCCESS':
                return {
                    ...state,
                    single: {
                        error: '',
                        loading: false,
                        record: state.single.record
                    }
                };
            case ducks_name + '.CREATE.ERROR':
                return {
                    ...state,
                    single: {
                        error: action.payload,
                        loading: false,
                        record: state.single.record
                    }
                };
            case ducks_name + '.UPDATE.REQUEST':
                return {
                    ...state,
                    single: {
                        error: '',
                        loading: true,
                        record: state.single.record
                    }
                };
            case ducks_name + '.UPDATE.SUCCESS':
                return {
                    ...state,
                    single: {
                        error: '',
                        loading: false,
                        record: state.single.record
                    }
                };
            case ducks_name + '.UPDATE.ERROR':
                return {
                    ...state,
                    single: {
                        error: action.payload,
                        loading: false,
                        record: state.single.record
                    }
                };
            case ducks_name + '.DELETE.REQUEST':
                return {
                    ...state,
                    single: {
                        error: '',
                        loading: true,
                        record: state.single.record
                    }
                };
            case ducks_name + '.DELETE.SUCCESS':
                return {
                    ...state,
                    single: {
                        error: '',
                        loading: false,
                        record: state.single.record
                    }
                };
            case ducks_name + '.DELETE.ERROR':
                return {
                    ...state,
                    single: {
                        error: action.payload,
                        loading: false,
                        record: state.single.record
                    }
                };

            //Local non-rest changes
            case ducks_name + '.SET':
                return {
                    ...state,
                    single: {
                        error: '',
                        loading: false,
                        record: {
                            ...state.single.record,
                            ...action.payload
                        }
                    }
                };
            case ducks_name + '.RESET':
                return {
                    ...state,
                    single: {
                        error: '',
                        loading: false,
                        record: {
                            ...record_default
                        }
                    }
                };

            default:
                return state;
        }
    };

    /**
    * State object
    * @typedef {Object} state - State of this type
    * @property {String} state.name - The type name
    * @property {Object} state.actions - Type actions
    * @property {Function} state.actions.[allRequest] - Get all records of a type
    * @property {Function} state.actions.[allSuccess] - Success when getting all records
    * @property {Function} state.actions.[allError] - Error when getting all records
    * @property {Function} state.actions.[someRequest] - Get paginated records of a type
    * @property {Function} state.actions.[someSuccess] - Success when getting paginated records
    * @property {Function} state.actions.[someError] - Error when getting paginated records
    * @property {Function} state.actions.[singleRequest] - Get one record of a type
    * @property {Function} state.actions.[singleSuccess] - Success when getting one record
    * @property {Function} state.actions.[singleError] - Error when getting one record
    * @property {Function} state.actions.[setRecord] - Set a local record
    * @property {Function} state.actions.[resetRecord] - Reset a local record
    */
    let state = {
        name: ducks_name,
        filter: filter,
        actions: actions,
        reducer
    }
    return state;
}
